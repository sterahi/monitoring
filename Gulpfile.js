var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    minifyCss   = require('gulp-minify-css'),
    concat      = require('gulp-concat')

gulp.task('styles', function() {
    gulp.src('./public/css/**/build.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./public/css'))
})

gulp.task('concat-js', function() {
    gulp.src(['./public/js/src/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/js'))
})

gulp.task('minify-css', function() {
    gulp.src('./public/css/main.css')
        .pipe(minifyCss({debug:true}, function(detail) {
            console.log(details.name + ': ' + details.stats.minifiedSize)
            }))
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest('./public/css/'))
})

gulp.task('default', function() {
    gulp.start('styles', 'concat-js', 'minify-css')
    gulp.watch(['./public/css/**/*.scss', './public/js/src/*.js'], ['styles', 'concat-js', 'minify-css'])
})

gulp.task('watch', function() {
    gulp.start('default')
})
